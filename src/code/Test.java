package code;

import com.nulooper.nlpir.NLPIRToolkit;

public class Test {

	public static void main(String[] args) {
		String sInput = "据悉，质检总局已将最新有关情况再次通报美方，要求美方加强对输华玉米的产地来源、运输"
				+ "及仓储等环节的管控措施，有效避免输华玉米被未经我国农业部安全评估并批准的转基因品系污染。";
		NLPIRToolkit.getInstance().init();
		String result = NLPIRToolkit.getInstance().processPrargraph(sInput, 1);
		System.out.println(result);
		System.out.println(NLPIRToolkit.getInstance().getKeyWords(sInput, 10,
				true));
		NLPIRToolkit.getInstance().exit();
	}
}