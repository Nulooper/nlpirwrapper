package com.nulooper.nlpir;

import com.sun.jna.Library;
import com.sun.jna.Native;

public class NLPIRToolkit {

	private static NLPIRToolkit instance = null;

	private NLPIRToolkit() {

	}

	public static NLPIRToolkit getInstance() {
		if (null == instance) {
			instance = new NLPIRToolkit();
		}
		return instance;
	}

	public void init() {
		try {
			String argu = "";
			// String system_charset = "GBK";//GBK----0
			String system_charset = "UTF-8";
			int charset_type = 1;
			// int charset_type = 0;
			// 调用printf打印信息
			int init_flag = NLPIRUtils.Instance.NLPIR_Init(
					argu.getBytes(system_charset), charset_type,
					"0".getBytes(system_charset));

			if (0 == init_flag) {
				System.err.println("初始化失败！");
				return;
			}
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}

	public String processPrargraph(String srcParagraph, int isPOSTagged) {
		return NLPIRUtils.Instance.NLPIR_ParagraphProcess(srcParagraph,
				isPOSTagged);
	}

	public double processFile(String sSourceFilename, String sResultFilename,
			int isPOStagged) {
		return NLPIRUtils.Instance.NLPIR_FileProcess(sSourceFilename,
				sResultFilename, isPOStagged);
	}

	public int importUserDict(String sFilename) {
		return NLPIRUtils.Instance.NLPIR_ImportUserDict(sFilename);
	}

	public int addUserWord(String sWord) {
		return NLPIRUtils.Instance.NLPIR_AddUserWord(sWord);
	}

	public int deleteUsrWord(String sWord) {
		return NLPIRUtils.Instance.NLPIR_DelUsrWord(sWord);
	}

	public int saveTheUsrDic() {
		return NLPIRUtils.Instance.NLPIR_SaveTheUsrDic();
	}

	public String getNewWords(String sFilename, int nMaxKeyLimit,
			boolean isWeightOut) {
		return NLPIRUtils.Instance.GetFileNewWords(sFilename, nMaxKeyLimit,
				isWeightOut);
	}

	public String getFileNewWords(String sFilename, int nMaxKeyLimit,
			boolean isWeightOut) {
		return NLPIRUtils.Instance.GetFileNewWords(sFilename, nMaxKeyLimit,
				isWeightOut);
	}

	public String getKeyWords(String sLine, int nMaxKeyLimit,
			boolean isWeightOut) {
		return NLPIRUtils.Instance.NLPIR_GetKeyWords(sLine, nMaxKeyLimit,
				isWeightOut);
	}

	public String getFileKeyWords(String sFilename, int nMaxKeyLimit,
			boolean isWeightOut) {
		return NLPIRUtils.Instance.GetFileKeyWords(sFilename, nMaxKeyLimit,
				isWeightOut);
	}

	public void exit() {
		NLPIRUtils.Instance.NLPIR_Exit();
	}

	private interface NLPIRUtils extends Library {
		// 定义并初始化接口的静态变量
		NLPIRUtils Instance = (NLPIRUtils) Native.loadLibrary(
				"resource/nlpir/NLPIR/NLPIR", NLPIRUtils.class);

		/**
		 * Return true if init succeed. Otherwise return false.
		 * 
		 * NOTE:The NLPIR_Init function must be invoked before any operation
		 * with NLPIR. The whole system need call the function only once before
		 * starting NLPIR. When stopping the system and make no more operation,
		 * NLPIR_Exit should be invoked to destroy all working buffer. Any
		 * operation will fail if init do not succeed.
		 * 
		 * Error reasons:NLPIR_Init fails mainly because of two reasons: 1)
		 * Required data is incompatible or missing 2) Configure file missing or
		 * invalid parameters. Moreover, you could learn more from the log file
		 * NLPIR.log in the default directory.
		 * 
		 * @param sDataPath
		 *            Initial Directory Path, where file Configure.xml and Data
		 *            directory stored. the default value is 0, it indicates the
		 *            initial directory is current working directory path. When
		 *            calling this function,
		 * @param encoding
		 *            encoding of input string, default is GBK_CODE (GBK
		 *            encoding), and it can be set with UTF8_CODE (UTF8
		 *            encoding) and BIG5_CODE (BIG5 encoding). 0 means GBK_CODE,
		 *            1 means UTF8_CODE, 2 means BIG5_CODE and 3 means
		 *            GBK_FANTI_CODE.
		 * @param sLicenceCode
		 *            license code, special use for some commercial users. Other
		 *            users ignore the argument
		 * @return
		 */
		public int NLPIR_Init(byte[] sDataPath, int encoding,
				byte[] sLicenceCode);

		/**
		 * Process a paragraph and return the processed result.
		 * 
		 * NOTE:The NLPIR_ParagraphProcess function works properly only if
		 * NLPIR_Init succeeds.
		 * 
		 * @param sSrc
		 *            The source paragraph.
		 * @param bPOSTagged
		 *            Judge whether need POS tagging, 0 for no tag; 1 for
		 *            tagging; default:1.
		 * @return 返回处理后的结果.
		 */
		public String NLPIR_ParagraphProcess(String sSrc, int bPOSTagged);

		/**
		 * 对用户指定为文件进行处理，将处理后的结果输出到指定的文件中.
		 * 
		 * @param sSourceFilename
		 *            The source file name to be analyzed;
		 * @param sResultFilename
		 *            The result file name to store the results.
		 * @param bPOStagged
		 *            Judge whether need POS tagging, 0 for no tag; 1 for
		 *            tagging; default:1.
		 * @return 若处理成功，则返回大于0.0的浮点数,否则返回0.0.Return the processing speed if
		 *         processing succeed. Otherwise return false.
		 */
		public double NLPIR_FileProcess(String sSourceFilename,
				String sResultFilename, int bPOStagged);

		/**
		 * Import user-defined dictionary from a text file.
		 * 
		 * NOTE:The NLPIR_ImportUserDict function works properly only if
		 * NLPIR_Init succeeds. The text dictionary file format see User-defined
		 * Lexicon. You only need to invoke the function while you want to make
		 * some change in your customized lexicon or first use the lexicon.
		 * After you import once and make no change again, NLPIR will load the
		 * lexicon automatically if you set UserDict "on" in the configure file.
		 * While you turn UserDict "off", user-defined lexicon would not be
		 * applied.
		 * 
		 * @param sFilename
		 *            Text filename for user dictionary.
		 * @return The number of lexical entry imported successfully.
		 */
		public int NLPIR_ImportUserDict(String sFilename);

		/**
		 * Add a word to the user dictionary.
		 * 
		 * NOTE:The NLPIR_AddUserWord function works properly only if NLPIR_Init
		 * succeeds.
		 * 
		 * @param sWord
		 *            the word to be added.
		 * @return Return 1 if add succeed. Otherwise return 0.
		 */
		public int NLPIR_AddUserWord(String sWord);

		/**
		 * Delete a word from the user dictionary.
		 * 
		 * NOTE:The NLPIR_DelUsrWord function works properly only if NLPIR_Init
		 * succeeds.
		 * 
		 * @param sWord
		 *            the word to be deleted.
		 * @return Return -1, the word not exist in the user dictionary; else,
		 *         the handle of the word deleted
		 */
		public int NLPIR_DelUsrWord(String sWord);

		/**
		 * Save the user dictionary to disk.
		 * 
		 * NOTE:The NLPIR_SaveTheUsrDic function works properly only if
		 * NLPIR_Init succeeds.
		 * 
		 * @return Return 1 if save succeed. Otherwise return 0.
		 */
		public int NLPIR_SaveTheUsrDic();

		/**
		 * Extract new words from paragraph.
		 * 
		 * @param sFilename
		 *            the input text.
		 * @param nMaxKeyLimit
		 *            the maximum number of key words.
		 * @param bWeightOut
		 *            whether the keyword weight output or not.
		 * @return Return the new words list if execute succeed. otherwise
		 *         return NULL. Format as: "科学发展观 宏观经济 " or
		 *         "科学发展观 23.80 宏观经济 12.20" with weight
		 */
		public String GetNewWords(String sFilename, int nMaxKeyLimit,
				boolean bWeightOut);

		/**
		 * Extract new words from a text file.
		 * 
		 * @param sFilename
		 *            the input text filename.
		 * @param nMaxKeyLimit
		 *            the maximum number of key words.
		 * @param bWeightOut
		 *            whether the keyword weight output or not.
		 * @return Return the keywords list if execute succeed. otherwise return
		 *         NULL. Format as: "科学发展观 宏观经济 " or "科学发展观 23.80 宏观经济 12.20"
		 *         with weight.
		 */
		public String GetFileNewWords(String sFilename, int nMaxKeyLimit,
				boolean bWeightOut);

		/**
		 * Extract keyword from paragraph.
		 * 
		 * @param sLine
		 *            the input text.
		 * @param nMaxKeyLimit
		 *            the maximum number of key words.
		 * @param bWeightOut
		 *            whether the keyword weight output or not.
		 * @return Return the keywords list if execute succeed. otherwise return
		 *         NULL. Format as: "科学发展观 宏观经济 " or "科学发展观 23.80 宏观经济 12.20"
		 *         with weight
		 */
		public String NLPIR_GetKeyWords(String sLine, int nMaxKeyLimit,
				boolean bWeightOut);

		/**
		 * Extract keyword from a text file.
		 * 
		 * @param sFilename
		 *            the input text filename.
		 * @param nMaxKeyLimit
		 *            the maximum number of key words.
		 * @param bWeightOut
		 *            whether the keyword weight output or not.
		 * @return Return the keywords list if execute succeed. otherwise return
		 *         NULL. Format as: "科学发展观 宏观经济 " or "科学发展观 23.80 宏观经济 12.20"
		 *         with weight
		 */
		public String GetFileKeyWords(String sFilename, int nMaxKeyLimit,
				boolean bWeightOut);

		/**
		 * Exit the program and free all resources and destroy all working
		 * buffer used in NLPIR.
		 * 
		 * NOTE:The NLPIR_Exit function must be invoked while stopping the
		 * system and make no more operation. And call NLPIR_Init function to
		 * restart NLPIR.
		 */
		public void NLPIR_Exit();
	}
}
